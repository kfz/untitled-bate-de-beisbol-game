extends StaticBody3D

var ang_vel = 0;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !$CollisionShape3D.disabled:
		print(ang_vel)
		rotation.y += ang_vel*delta
		ang_vel -= 22.5*delta
		if rotation.y < 0:
			$CollisionShape3D.disabled = true
			ang_vel = 0; 

func _input(event):
	if event.is_action_pressed("bat"):
		if $CollisionShape3D.disabled:
			$CollisionShape3D.disabled = false
			ang_vel = 10
