extends AnimationPlayer

func _set_animation(animation: Animation, track_index: int, bottom: float, top: float, time: float, keyframes: int) -> Animation:
	var squared_sine_wave = func(x: float) -> float: return pow(sin(x), 2)
	# some aliases for clarity. 
	var amplitude = top - bottom
	var y_offset = bottom
	var period = time
	var increment = time/keyframes
	animation.set_step(increment)
	var math_func = func(x: float) -> Vector3: 
		return Vector3(0, amplitude*squared_sine_wave.call(x*PI/(period) + PI/4)+y_offset, 0)
	for i in range(keyframes):
		animation.track_insert_key(track_index, i*increment, math_func.call(i*increment))
	return animation
	
# Called when the node enters the scene tree for the first time.
func _ready():
	var time = 6
	var animation = Animation.new()
	var animation_library = self.get_animation_library("")
	var track_index = animation.add_track(Animation.TYPE_POSITION_3D)
	animation.track_set_path(track_index, "MeshInstance3D:position")
	animation.set_length(time)
	animation.set_loop_mode(Animation.LOOP_LINEAR)
	_set_animation(animation, track_index, 1.2, 1.8, time, 60)
	animation_library.add_animation("Hover", animation)
	self.play("Hover")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
