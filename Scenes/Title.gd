extends Control

@export var starting_scene :PackedScene;
@export var quit_target_texts :Array[String];
@onready var _quit_button :Button = $"VBoxContainer/ExitButton";


func _ready() -> void:
	_set_quit_button_caption()
	

func _on_start_button_pressed() -> void:
	get_tree().change_scene_to_packed(starting_scene)
	

func _on_exit_button_pressed() -> void:
	get_tree().quit()


func _set_quit_button_caption():
	_quit_button.text = "Salir a " + quit_target_texts[randi() % quit_target_texts.size()];
	pass
