extends Control

@export var title_scene :String;

func _process(_delta: float) -> void:
	if get_tree().paused and Input.is_action_just_released("ui_cancel"):
		_activate_pause_screen(false)
	elif Input.is_action_just_released("ui_pause"):
		_activate_pause_screen(true)
		
		
func _activate_pause_screen(paused :bool) -> void:
	get_tree().paused = paused
	self.visible = paused


func _on_continue_button_pressed() -> void:
	_activate_pause_screen(false)


func _on_exit_button_pressed() -> void:
	get_tree().change_scene_to_file(title_scene);
	get_tree().paused = false
