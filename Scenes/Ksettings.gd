extends PanelContainer

@export var deactivation_time_s :float;
@onready var _deactivate_game_button :Button = $VBoxContainer/HBoxContainer2/DeactivateGameButton;


func _process(_delta: float) -> void:
	if Input.is_action_just_released("ui_cancel"):
		self.visible = false;
	

func _on_settings_button_pressed() -> void:
	self.visible = true;


func _on_back_button_pressed() -> void:
	self.visible = false;


func _on_deactivate_game_button_pressed() -> void:
	_deactivate_game_button.text = "Desactivado";
	await get_tree().create_timer(deactivation_time_s).timeout;
	get_tree().quit();	
