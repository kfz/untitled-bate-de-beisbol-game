extends Node3D

@export var shield_number = 5
@export var max_shields = 8
@export var shield_distance = 1.4
@export var animation_offset = 6.0/max_shields
var current_shield_number = 0
var shield = preload("res://Scenes/shield.tscn")

func _attach_shield():
	var shield_instance: StaticBody3D = shield.instantiate()
	var shield_index = pow(-1, current_shield_number+1)*round(current_shield_number/2.0)
	# the index goes -0, then 1, then -1, then 2, then -2, etc
	var angle = shield_index*2*PI/max_shields
	shield_instance.rotate_y(angle) # right, then left, then right again
	shield_instance.translate_object_local(Vector3(0, 0, shield_distance))
	var animation_player: AnimationPlayer = shield_instance.get_node("AnimationPlayer")
	add_child(shield_instance)
	animation_player.seek(animation_offset*shield_index)
	current_shield_number += 1

# Called when the node enters the scene tree for the first time.
func _ready():
	while current_shield_number < shield_number:
		_attach_shield()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
